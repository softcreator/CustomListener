package com.salman.customlistener;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    int count = 0;
    Button btn;
//    EditText edittext_first_screen;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
//        edittext_first_screen = (EditText) findViewById(R.id.editText);

        Button btn = (Button) findViewById(R.id.button);
        btn.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
//        String str = edittext_first_screen.getText().toString();

//
//        Bundle bundle =  new Bundle();
//        bundle.putString("edit value", str);
      count++;


        Toast.makeText(MainActivity.this,
                "The button has been clicked " + count +     " times", Toast.LENGTH_SHORT).show();
    }
}